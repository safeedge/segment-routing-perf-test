#!/usr/bin/env python

"""
This profile is a chain of PCs with segment routing enabled. No rules are configured: You will have to do that yourself.

Instructions:
TBD

"""

import geni.portal as portal
import geni.rspec.pg as pg

#
# This geni-lib script is designed to run in the Cloudlab or Emulab Portal.
#
pc = portal.Context()


GIGABIT_BANDWIDTH = 1000*1000

pc.defineParameter("osNodeType", "Hardware Type",
                   portal.ParameterType.NODETYPE, "",
                   longDescription="A specific hardware type to use for each node.  Cloudlab clusters all have machines of specific types.  When you set this field to a value that is a specific hardware type, you will only be able to instantiate this profile on clusters with machines of that type.  If unset, when you instantiate the profile, the resulting experiment may have machines of any available type allocated.")
pc.defineParameter(name="num_nodes",
                   description="Number of nodes",
                   typ=portal.ParameterType.INTEGER,
                   defaultValue=6,
                   )

params = pc.bindParameters()

image_urn = 'utah.cloudlab.us'
image_project = 'cs6480-2017-cl' # I need to move this to the Safeedge emulab project...
image_os = 'Ubuntu_16.04.04'
image_tag = 'FRR'

disk_image = 'urn:publicid:IDN+{urn}+image+{proj}-PG0:{os}_{tag}'.format(urn=image_urn, proj=image_project, os=image_os, tag=image_tag)

request = pc.makeRequestRSpec()

# Request nodes
nodes = []
for i in range(0, params.num_nodes):
    node = request.RawPC("node" + str(i))
    node.addNamespace(pg.Namespaces.EMULAB)
    node.disk_image = disk_image
    node.hardware_type = params.osNodeType
    nodes.append(node)

# Build LANs between adjacent nodes
for i in range(1, len(nodes)):
  lhs_node = nodes[i-1]
  rhs_node = nodes[i]
  
  lhs_interface_name = "if-{rhs}".format(rhs=rhs_node.client_id)
  rhs_interface_name = "if-{lhs}".format(lhs=lhs_node.client_id)
  
  lhs_interface = lhs_node.addInterface(lhs_interface_name)
  rhs_interface = rhs_node.addInterface(rhs_interface_name)
  
  link = request.LAN("{lhs}-{rhs}".format(lhs=lhs_node.client_id, rhs=rhs_node.client_id))
  link.bandwidth = GIGABIT_BANDWIDTH
  
  link.addInterface(lhs_interface)
  link.addInterface(rhs_interface)

pc.printRequestRSpec(request)
